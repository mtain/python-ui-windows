# -*- coding: utf-8 -*-
import os

class NetworkTools:
    @staticmethod
    def getInfo():
        ipconfig = os.popen("ipconfig /all").read()
        return ipconfig
